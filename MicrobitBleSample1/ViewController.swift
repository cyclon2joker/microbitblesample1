//
//  ViewController.swift
//  MicrobitBleSample1
//
//  Created by pies on 2017/12/17.
//  Copyright © 2017年 pies. All rights reserved.
//
import UIKit
import CoreBluetooth

class ViewController: UIViewController,
    CBCentralManagerDelegate,CBPeripheralDelegate,
    UITableViewDataSource, UITableViewDelegate {
    let CMD_LEN = 10
    let hogeServiceUUId = CBUUID(string: "ED3A76F6-70B5-4BBE-840D-EAA341F78417")
    let testDeviceName = "Micro:bit TestDevice Hoge"
    
    
    @IBOutlet weak var tblLog: UITableView!
    
    @IBOutlet weak var btnScan: UIButton!
    
    @IBOutlet weak var btnConnectDevice: UIButton!
    
    @IBOutlet weak var btnTestSend: UIButton!
    
    @IBOutlet weak var btnDisConnectDevice: UIButton!
    
    
    var logMsg = [String]()
    
    var cm: CBCentralManager!
    var hogeDevice: CBPeripheral?
    var hogeService: CBService?
    var hogeChar: CBCharacteristic?
    
    var selectedCmdIdx: Int = -1
    
    override func viewDidLoad() {
        
        super.viewDidLoad()

        btnScan.isEnabled = false
        btnConnectDevice.isEnabled = false
        btnTestSend.isEnabled = false
        btnDisConnectDevice.isEnabled = false
        
        tblLog.dataSource = self
        tblLog.delegate = self
        // ble初期化
        cm = CBCentralManager(delegate: self, queue:nil)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func tapBtnScan(_ sender: Any) {
        
        if !cm.isScanning {
            // BLEデバイスの検出を開始.
            cm.scanForPeripherals(withServices: [hogeServiceUUId], options: nil)
            btnScan.isEnabled = false
            btnConnectDevice.isEnabled = false
            hogeDevice = nil
            logMsg.append("スキャン開始")
            reloadAndLast()
        }
    }
    
    // micro:bitと接続
    @IBAction func tapBtnConnectDevice(_ sender: Any) {
        
        if let tgtDevice = hogeDevice {
            btnConnectDevice.isEnabled = false
            logMsg.append("接続開始")
            cm.connect(tgtDevice, options: nil)
            reloadAndLast()
        }
    }
    
    // テキトー送信
    @IBAction func tapBtnTestSend(_ sender: Any) {
        if let tgtDevice = hogeDevice {
            if hogeService != nil && hogeChar != nil {
                tgtDevice.writeValue(toCmd("hoge"), for: hogeChar!, type: CBCharacteristicWriteType.withResponse)
                logMsg.append("送信:hoge")
                reloadAndLast()
                hogeDevice?.readValue(for: hogeChar!)
            }
        }
    }
    
    
    // 固定長にしてbyte array化
    func toCmd(_ cmd: String) -> Data {
        var buf = "\(cmd)"
        while buf.utf8.count < CMD_LEN {
            buf = "\(buf) "
        }
        print("[\(buf)]")
        var arr = [UInt8]()
        for c in buf.utf8 {
            arr.append(c)
        }
        // 相手はC言語の世界なのでケツにnull。。。
        arr.append(0x0)
        return Data(bytes: arr)
    }
    
    @IBAction func tapBtnDisconnect(_ sender: Any) {
        btnTestSend.isEnabled = false
        if hogeDevice != nil {
            btnDisConnectDevice.isEnabled = false
            cm.cancelPeripheralConnection(hogeDevice!)
            logMsg.append("切断実行")
            reloadAndLast()
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return logMsg.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CELL_LOG")!
        let lblLog = cell.contentView.viewWithTag(1001) as! UILabel
        lblLog.text = logMsg[indexPath.item]
        return cell
    }
    
    func reloadAndLast() {
        tblLog.reloadData()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) { [unowned self] in
            let idx = IndexPath(item: self.logMsg.count - 1, section: 0)
            self.tblLog.scrollToRow(at: idx, at:.bottom, animated: true)
        }
    }
    
    
    
    func centralManagerDidUpdateState(_ central: CBCentralManager) {        
        print("state \(central.state)")
        switch (central.state) {
        case .poweredOff:
            print("Bluetoothの電源がOff")
            logMsg.append("Bluetooth:電源Off")
            btnScan.isEnabled = false
            btnConnectDevice.isEnabled = false
        case .poweredOn:
            print("Bluetoothの電源はOn")
            logMsg.append("Bluetooth:電源On")
            
            if !btnScan.isEnabled && !cm.isScanning {
                btnScan.isEnabled = true
            }
        case .resetting:
            print("レスティング状態")
            logMsg.append("Bluetooth:レスティング状態")
        case .unauthorized:
            print("非認証状態")
            logMsg.append("Bluetooth:非認証状態")
        case .unknown:
            print("不明")
            logMsg.append("Bluetooth:不明")
        case .unsupported:
            print("非対応")
            logMsg.append("Bluetooth:非対応")
            
        }
        tblLog.reloadData()
    }
    
    
    
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        
        if let dName = advertisementData[CBAdvertisementDataLocalNameKey] as? String {
            
            print("device:\(dName)")
            
            // デバイス名確認
            if dName.hasPrefix("Micro:bit HogeDevice") {
                // サービス確認
                if let srvIds = advertisementData[CBAdvertisementDataServiceUUIDsKey] as? [Any] {
                    if srvIds.filter({$0 as? CBUUID == hogeServiceUUId}).count > 0 {
                        // hoge service確認
                        cm.stopScan()
                        logMsg.append("Micor:bit確認。スキャン停止")
                        print(CBAdvertisementDataServiceUUIDsKey)
                        logMsg.append("device:\(dName)\nuuid:\(peripheral.identifier)")
                        tblLog.reloadData()
                        self.hogeDevice = peripheral
                        btnScan.isEnabled = true
                        btnConnectDevice.isEnabled = true
                    } else {
                        logMsg.append("Micor:bit確認。hogeサービスなし")
                    }
                }
            }
        }
        reloadAndLast()
        
        
        // peripheral.
        
        //        cm.stopScan()
    }
    
    // peripheralと接続成功
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        
        print("デバイスと接続成功")
        logMsg.append("デバイスと接続成功")
        
        // デバイスのdelegate設定
        peripheral.delegate = self
        
        // サービスのUUIDを確認
        peripheral.discoverServices([hogeServiceUUId])
        
        reloadAndLast()
    }
    
    // peripheralと接続失敗
    func centralManager(_ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral, error: Error?) {
        
        print("デバイスと接続失敗,\ncause:\(error)")
        logMsg.append("デバイスと接続失敗")
        //logMsg.append("error:\(error)")
        
        btnConnectDevice.isEnabled = true
        btnTestSend.isEnabled = false
        reloadAndLast()
        
    }
    func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
        
        btnTestSend.isEnabled = false
        btnDisConnectDevice.isEnabled = false
        print("デバイス切断")
        logMsg.append("デバイス切断")
        if error == nil {
            btnConnectDevice.isEnabled = true
        } else {
            print("cause:\(error!)")
        }
        reloadAndLast()
    }
    
    // peripheralサービス検索結果
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        
        if error != nil {
            // サービス確認失敗
            logMsg.append("サービス確認失敗")
            print("サービス確認失敗\ncause:\(error!)")
        } else {
            // サービス確認成功
            logMsg.append("サービス確認成功")
            logMsg.append("Characteristics確認実行")
            btnTestSend.isEnabled = true
            btnDisConnectDevice.isEnabled = true
            
            hogeService = peripheral.services?.filter({$0.uuid == hogeServiceUUId})[0]
            peripheral.discoverCharacteristics([hogeServiceUUId], for: hogeService!)
        }
        
        reloadAndLast()
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        if error != nil {
            logMsg.append("Characteristics確認:エラー")
            print("Characteristics確認:エラー")
            print("cause:\(error!)")
        } else {
            logMsg.append("Characteristics確認:成功")
            hogeChar = service.characteristics!.filter({$0.uuid == hogeServiceUUId})[0]
            logMsg.append("Characteristics読込リクエスト")
            peripheral.readValue(for: hogeChar!)
            btnTestSend.isEnabled = true
        }
        reloadAndLast()
    }
    
    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
        if error != nil {
            logMsg.append("Characteristics読込:エラー")
            print("Characteristics読込:エラー")
            print("cause:\(error!)")
            
            
        } else {
            logMsg.append("Characteristics読込:成功")
            print("Characteristics:\(characteristic.value)")
            if let data = characteristic.value {
                let str = String(data: data, encoding: .utf8)
                print("receive:\(str)")
            }
        }
        reloadAndLast()
    }
    
    func peripheral(_ peripheral: CBPeripheral, didWriteValueFor characteristic: CBCharacteristic, error: Error?) {
        if error != nil {
            
            print(error!)
            
        } else {
            if let data = characteristic.value {
                let str = String(data: data, encoding: .utf8)
                print("receive:\(str)")
            }
        }
    }
}
